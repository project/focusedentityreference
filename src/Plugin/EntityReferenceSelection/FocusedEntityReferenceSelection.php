<?php

namespace Drupal\focusedentityreference\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\focusedentityreference\Ajax\UpdateOptionsCommand;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\quickedit\Form\QuickEditFieldForm;
use Drupal\taxonomy\Entity\Term;

/**
 * Provides an entity reference selection to focus options from other fields.
 *
 * @EntityReferenceSelection(
 *   id = "focusedentityreference",
 *   label = @Translation("More focused selections from already selected by other entity reference fields"),
 *   group = "focusedentityreference",
 *   weight = 0
 * )
 */
class FocusedEntityReferenceSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'parent_fields' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $configuration = $this->getConfiguration();
    $entity_type_id = $configuration['target_type'];
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);

    unset($form['target_bundles']);
    unset($form['target_bundles_update']);

    if ($entity_type->entityClassImplements(FieldableEntityInterface::class)) {
      $fields = [];
      foreach (array_keys($bundles) as $bundle) {
        $bundle_fields = array_filter($this->entityFieldManager->getFieldDefinitions($entity_type_id, $bundle), function ($field_definition) {
          return !$field_definition->isComputed();
        });
        foreach ($bundle_fields as $field_name => $field_definition) {
          /* @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
          $columns = $field_definition->getFieldStorageDefinition()->getColumns();
          // If there is more than one column, display them all, otherwise just
          // display the field label.
          // @todo: Use property labels instead of the column name.
          if (count($columns) > 1) {
            foreach ($columns as $column_name => $column_info) {
              $fields[$field_name . '.' . $column_name] = $this->t('@label (@column)', ['@label' => $field_definition->getLabel(), '@column' => $column_name]);
            }
          }
          else {
            $fields[$field_name] = $this->t('@label', ['@label' => $field_definition->getLabel()]);
          }
        }
      }

      $form['sort']['field'] = [
        '#type' => 'select',
        '#title' => $this->t('Sort by'),
        '#options' => $fields,
        '#ajax' => TRUE,
        '#empty_value' => '_none',
        '#sort_options' => TRUE,
        '#limit_validation_errors' => [],
        '#default_value' => $configuration['sort']['field'],
      ];
      if ($entity_type->hasKey('bundle')) {
        $form['sort']['field']['#states'] = [
          'visible' => [
            ':input[name^="settings[handler_settings][target_bundles]["]' => ['checked' => TRUE],
          ],
        ];
      }

      $form['sort']['settings'] = [
        '#type' => 'container',
        '#attributes' => ['class' => ['entity_reference-settings']],
        '#process' => [[EntityReferenceItem::class, 'formProcessMergeParent']],
      ];

      if ($configuration['sort']['field'] != '_none') {
        $form['sort']['settings']['direction'] = [
          '#type' => 'select',
          '#title' => $this->t('Sort direction'),
          '#required' => TRUE,
          '#options' => [
            'ASC' => $this->t('Ascending'),
            'DESC' => $this->t('Descending'),
          ],
          '#default_value' => $configuration['sort']['direction'],
        ];
      }
    }

    $form['auto_create'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Create referenced entities if they don't already exist"),
      '#default_value' => $configuration['auto_create'],
      '#weight' => -2,
    ];

    if ($entity_type->hasKey('bundle')) {
      $bundles = array_intersect_key($bundle_options, array_filter((array) $configuration['target_bundles']));
      $form['auto_create_bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Store new items in'),
        '#options' => $bundles,
        '#default_value' => $configuration['auto_create_bundle'],
        '#access' => count($bundles) > 1,
        '#states' => [
          'visible' => [
            ':input[name="settings[handler_settings][auto_create]"]' => ['checked' => TRUE],
          ],
        ],
        '#weight' => -1,
      ];
    }

    return $form;
  }

  /**
   * Update the dependent field options.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The updated field.
   */
  public static function updateDependentField(array $form, FormStateInterface $form_state) {
    $entity = FocusedEntityReferenceSelection::getEntityFromForm($form_state);
    $subform = $form;
    $trigger_field = $form_state->getTriggeringElement();

    // Inline form marker.
    $inline_form = FALSE;
    if (isset($trigger_field['#parents'])) {
      $parents = $trigger_field['#parents'];
      // If paragraphs are used, find the most deeply nested paragraph.
      while (TRUE) {
        if (!in_array('subform', $parents, TRUE)) {
          break;
        }
        $parent_field_key = array_shift($parents);
        /** @var \Drupal\field\Entity\FieldConfig $definition */
        $definition = $entity->getFieldDefinition($parent_field_key);
        if ($definition->getSetting('target_type') !== 'paragraph') {
          break;
        }
        $inline_form = TRUE;
        $delta = array_shift($parents);
        $widget = $subform[$parent_field_key]['widget'][$delta];
        if (!isset($widget['#paragraph_type'])) {
          break;
        }
        // Create a new paragraph instead of loading the actual paragraphs here,
        // as the actual paragraph can be empty.
        $entity = \Drupal::entityTypeManager()->getStorage('paragraph')->create([
          'type' => $widget['#paragraph_type'],
        ]);

        $subform = $subform[$parent_field_key]['widget'][$delta]['subform'];
        // Remove 'subform' corresponding with the current paragraph from array.
        array_shift($parents);
      }
    }

    // Update children.
    $children = $trigger_field['#ajax']['focusedentityreference_children'];
    $field_definitions = $entity->getFieldDefinitions();
    $response = new AjaxResponse();
    foreach ($children as $child) {
      if ($field_definitions[$child]->getSetting('handler') == 'focusedentityreference') {
        $parent_field_value = $trigger_field['#value'];
        // If the field widget is entity autocomplete, the returned value is a.
        if ($trigger_field['#type'] === 'entity_autocomplete' && preg_match('/\((\d )\)$/', $parent_field_value, $matches)) {
          // String which contains the entity id.
          $parent_field_value = $matches[1];
        }

        if (!is_array($parent_field_value)) {
          $parent_field_value = [$parent_field_value];
        }

        $options = [];
        // This is almost the same as optionsFromTids but we need a different format.
        foreach ($parent_field_value as $tid) {
          $term = Term::load($tid);
          // Nesting was not working in the JavaScript so we are skipping it.
          // $vocab_id = FocusedEntityReferenceSelection::vocabIdFromTerm($term);
          // str_repeat('-', $term->depth) .
          if ($term) {
            $options[] = [
              'key' => $tid,
              'value' => \Drupal::service('entity.repository')->getTranslationFromContext($term)->label(),
            ];
          }
        }

        $form_field = $subform[$child];
        $form_field['widget']['#options'] = $options;
        $html_field_id = explode('-wrapper-', $form_field['#id'])[0];

        // Fix html_field_id last char when it ends with _.
        $html_field_id = substr($child, strlen($child) - 1, 1) == '_' ? $html_field_id . '-' : $html_field_id;

        $formatter = $form_field['widget']['#type'];
        // Check if field is multiple or not.
        $multiple = FALSE;
        /**
         * @var \Drupal\field\Entity\FieldStorageConfig $storage_config
         */
        $storage_config = $field_definitions[$child]->getFieldStorageDefinition();
        if ($storage_config->getCardinality() !== 1) {
          $multiple = TRUE;
        }
        $response->addCommand(new UpdateOptionsCommand($html_field_id, $options, $formatter, $multiple));
      }
    }
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $parent_field_tids = $this->getPossibleValues();
    return $this::optionsFromTermIds($parent_field_tids, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function countReferenceableEntities($match = NULL, $match_operator = 'CONTAINS') {
    // @TODO fix our options are nested so this will be wrong.
    return count($this->getReferenceableEntities($match, $match_operator));
  }

  /**
   * Get the field names for the parent fields from configuration.
   *
   * @TODO do that right now we're hard-coded for expediency in development.
   */
  public function getParentFields() {
    $config = $this->getConfiguration();
    $parent_fields = $config['parent_fields'];
    if (!$parent_fields) {
      return [
        'field_findit_activities',
        'field_findit_services',
      ];
    }
    return $parent_fields;
  }

  /**
   * Returns all possible values from the vocabularies used for the parent fields.
   *
   * We really did not want any function like this, we shouldn't need it, but
   * we do because there is apparently no reasonable way to override the
   * validation for a field, so we need to have the field
   */

  /**
   * Return the so-far selected values from all available parent fields.
   */
  public function getPossibleValues($entity = NULL) {
    $values = [];
    if ($entity) {
      // An entity was passed in and we presume it to be valid.
    }
    elseif (isset($this->configuration['entity']) && $this->configuration['entity'] instanceof EntityInterface) {
      $entity = $this->configuration['entity'];
    }
    else {
      return $values;
    }
    if (!$parent_fields = $this->getParentFields()) {
      return $values;
    }
    foreach ($parent_fields as $parent_field) {
      // We have meaningless keys here so NO to the += operator.
      $values = array_merge($values, $this::getFieldValue($entity, $parent_field));
    }
    return $values;
  }

  /**
   * Get the parent field value.
   *
   * @param \Drupal\Core\Entity\Entity|null $entity
   *   The fallback entity to extract the value from.
   *
   * @return mixed
   *   The field value.
   */
  public static function getFieldValue(EntityInterface $entity, string $field_name) {
    $refined_value = [];
    // In refactoring the code from Business Rules, a couple of WTFs.
    //  - The conditional for getting a value from a paragraph is identical to
    //    non-paragraphs, unless i guess the expected handed-in value is not
    //    the $this->configuration['entity'] value… but that's what we hand in.
    //  - Why "getString()" to get an array?  And why does it seem to work?!?
    //    It produces a result like "67, 90, 92" and i maybe lost the explode.
    //    Ah that happens in getReferenceableEntities() we'll move it here so
    //    the output of this function is consistent.
    //  - Why in one part of the code does it check for $value['target_id'] and
    //    in another it checks for $value[0]['target_id']?  Whatever for that
    //    we'll just test both.
    $raw = $entity->get($field_name)->getValue();

    if (!is_array($raw)) {
      // Something unexpected happened.
      return $refined_value;
    }

    // In case we don't get an array of multiple values, make consistent as if.
    if (!isset($raw[0])) {
      $raw = [0 => $raw];
    }

    foreach ($raw as $value) {
      if (is_array($value) && !empty($value['target_id']) && preg_match('/\((\d+)\)$/', $value['target_id'], $matches)) {
        // If the field widget is entity autocomplete, the returned value is a
        // string which contains the entity id.
        $refined_value[] = $matches[1];
      }
      elseif (is_array($value) && !empty($value['target_id'])) {
        $refined_value[] = $value['target_id'];
      }
    }

    return $refined_value;
  }

  /**
   * Helper function to extract the associated entity from form or context data.
   *
   * Returns a stdClass object for entity if no match is found.
   *
   * This logic extracted directly from business_rules module.
   */
  public static function getEntityFromForm($form_state, $context = NULL) {
    // Establish whether we're in a normal EntityForm context or an inline
    // QuickeditFieldForm context and retrieve the entity from the respectively
    // appropriate place.

    // @todo check patch inline_entity_form_2973571_1.patch.
    $form_object = $form_state->getFormObject();

    if (isset($context['items']) && $context['items'] instanceof   EntityReferenceFieldItemListInterface) {
      /**
       * @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items
       */
      $items = $context['items'];
      $entity = $items->getEntity();
    }
    elseif ($form_object instanceof EntityFormInterface) {
      $entity = $form_object->getEntity();
    }
    elseif ($form_object instanceof QuickEditFieldForm) {
      $entity = $form_state->getBuildInfo()['args'][0];
    }
    else {
      $entity = new \stdClass();
    }

    return $entity;
  }

  /**
   * Returns array of options ready for a select element.
   *
   * Optionally nested by vocabulary but we had to turn this off because
   * JavaScript's element.options.add(new Option) is not working with it.
   */
  public static function optionsFromTermIds($tids, $nest = FALSE) {
    $options = [];
    $terms = \Drupal\taxonomy\Entity\Term::loadMultiple($tids);
    foreach ($terms as $term) {
      if ($nest) {
        $vocab_id = FocusedEntityReferenceSelection::vocabIdFromTerm($term);
        $options[$vocab_id][$term->id()] = str_repeat('-', $term->depth) . \Drupal::service('entity.repository')->getTranslationFromContext($term)->label();
      }
      else {
        $options[$term->id()] = \Drupal::service('entity.repository')->getTranslationFromContext($term)->label();
      }
    }
    return $options;
  }

  /**
   * Returns vocabulary name given a term.
   *
   * @TODO do something more clever if the term belongs to more than one
   * vocabulary.  Probably at least picking a vocab that our target fields
   * allow.  For now this covers the 99.99% use case.
   */
  public static function vocabIdFromTerm($term) {
    return $term->vid->first()->getValue()['target_id'];
  }
}
